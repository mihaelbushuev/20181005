Нужно написать скрипт HeikinAshiColor.php?stock=tqqq который определяет цвет торгового индикатора Heikin Ashi и отправляет email через api sendgrid на yurius86@gmail.com
Через $_GET[stock] передаем название акции.

Цены прасятся со странички: https://finance.yahoo.com/quote/TQQQ/history?p=TQQQ&.tsrc=fin-srch

В квадратных скобках цифры с картинки

HAO = (Open of previous bar [72.06] + Close of previous bar [71.35]) / 2
HAC = (Open [71.82]  + High [72.95] + Low [71.35] + цена в данный момент [72.30]) / 4

Если HAC>HAO то выводим green, отправляем имейл green.
Иначе выводим red, отправляем имейл red.

В результате Вы должны предоставить ссылку вида:
http://test.com/HeikinAshiColor.php?stock=tqqq

Вместо tqqq можно подставить любую другую акцию.

Проверить правильность можно с помощью:
https://www.tradingview.com/chart/?symbol=TQQQ

Цвета столбиков heikin ashi должны совпадать с тем что выводит Ваш скрипт.

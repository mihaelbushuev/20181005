<?php // You have to pay for everything. Not money, so time.

	$stock = $_GET['stock'];

	$opts = array(
		// URL
		'url' => 'https://finance.yahoo.com/quote/' . $stock . '/history?p=' . $stock . '&.tsrc=fin-srch',
		// Parsing rules
		'rules' => array(
			// Curent value rule
			'current' => '//span[@data-reactid="35"]',
			// History values rule
			'tables' => '//tbody[@data-reactid="50"]',
		),
		// Email
		'email' => array(
			// Subject
			'subject' => 'Heikin Ashi Color',
			// Message
			'message' => '',
			// Result sender
			'sender_name' => 'Mihael Bushuev',
			'sender_email' => 'mihael.bushuev@gmail.com',
			// Result reciver
			'reciver_name' => 'HR',
			'reciver_email' => 'yurius86@gmail.com',
			// SendGrid Api Key
			'api_key' => '_YOUR_API_KEY_',
		),
	);

	$html = stm_get_html( $opts['url'] );

	if ( $html === false ) {

		print 'Something trouble...';

	} else {

		// Parsing
		$values = stm_parse_values( $html, $opts['rules'] );

		// HAO
		$HAO = ( $values['open_prev'] + $values['close_prev'] ) / 2;
		// HAC
		$HAC = ( $values['open'] + $values['high'] + $values['low'] + $values['current'] ) / 4;

		// Compare
		if ( $HAC > $HAO ) {
			$opts['email']['message'] = 'green';
		} else {
			$opts['email']['message'] = 'red';
		}

		$send = stm_send_mail( $opts['email'] );

	}

function stm_send_mail( $mail ) {

	include("sendgrid-php/sendgrid-php.php");

	$email = new \SendGrid\Mail\Mail();
	$email->setFrom( $mail['sender_email'], $mail['sender_name'] );
	$email->setSubject( $mail['subject'] );
	$email->addTo( $mail['reciver_email'], $mail['reciver_name'] );
	$email->addContent( "text/plain", $mail['message']);
	$email->addContent(
	    "text/html", $mail['message']
	);
	$sendgrid = new \SendGrid( $mail['api_key'] );
	try {
	    $response = $sendgrid->send($email);
	    $res = $response->statusCode();
	} catch (Exception $e) {
	    $res = 'Caught exception: '. $e->getMessage();
	}

	return $res;

}

function stm_get_html( $url ) {

	// Create a stream
	$stream_opts = array(
		'http' => array(
			'method' => "GET",
			'header' => "Accept-language: ru\r\n" .
					    "Cookie: foo=bar\r\n",
			'timeout' => 4,
			'ignore_errors' => true,
		),
	);
	$context = stream_context_create( $stream_opts );

	$html = file_get_contents( $url, false, $context );

	$headers = get_headers( $url );

	if ( ! empty( $headers[0] ) ) {
		preg_match( '/\d{3}/', $headers[0], $matches);
		$httpCode = $matches[0] . PHP_EOL;
	}

	if ( $httpCode != 200 ) {
		$html = false;
	}

	return $html;

}

function stm_parse_values( $html, $rules ) {

	// Values array
	$values = array();

	// Create a DOMDocument
	$dom = new DOMDocument();

	// Load html
	$dom->loadHTML( mb_convert_encoding( $html, 'HTML-ENTITIES', 'UTF-8' ) );

	// Create a DOMXPath
	$xpath = new DOMXPath( $dom );

	// Find current value
	$currents = $xpath->query( $rules['current'] );
	// Current value
	$values['current'] = $currents->item(0)->nodeValue;

	// Find history values
	$tables = $xpath->query( $rules['tables'] );

	// Table history values
	$rows = $tables->item(0)->childNodes;

	// Row last values
	$cols0 = $rows->item(0)->childNodes;
	// Row previous values
	$cols1 = $rows->item(1)->childNodes;

	// Last open value
	$values['open'] = $cols0->item(1)->nodeValue;
	// Last high value
	$values['high'] = $cols0->item(2)->nodeValue;
	// Last low value
	$values['low'] = $cols0->item(3)->nodeValue;
	// Previous open value
	$values['open_prev'] = $cols1->item(1)->nodeValue;
	// Previous close value
	$values['close_prev'] = $cols1->item(4)->nodeValue;

	return $values;

}
